pipeline {
    agent any

    environment {
        REGION = 'ap-south-1'
        K8S_CLUSTER_NAME = 'EKS-ExternalTraining-Account2'
        ECR_REGISTRY = '045044665512.dkr.ecr.ap-south-1.amazonaws.com'
        ECR_REPOSITORY = 'spacex-netflix-repo'
        ARGOCD_SECRET = credentials('argocd-secret')
        TMDB_SECRET = credentials('tmdb-token-secret')
        ARGOCD_NAMESPACE = "spacex-argocd"
        PREFIX = "nayan"
        CODE_REPO_NAME = "spacex-netflix"
        CODE_REPO_URL = "https://gitlab.com/nayan98/spacex-netflix.git"
        MANIFESTS_REPO_NAME = "spacex-netflix-manifests"
        MANIFESTS_REPO_URL = "https://gitlab.com/nayan98/spacex-netflix-manifests.git"
    }
    
    stages {
        stage('Checkout Code') {
            steps {
                withCredentials([gitUsernamePassword(credentialsId: 'nayan-gitlab', gitToolName: 'git-tool')]) {
                    sh '''
                        git clone ${CODE_REPO_URL} -b ${gitlabBranch}
                    '''
                }
            }
        }

        stage('Create Image Tag') {
            steps {     
                dir("${env.WORKSPACE}/${env.CODE_REPO_NAME}") {  
                    script {
                        short_sha =  sh (script: 'git rev-parse --short=7 HEAD:app', returnStdout: true).trim()
                        env.IMAGE_TAG = short_sha
                    }
                }
            }
        }

        stage('Build and Publish Image') {
            steps {
                sh '''
                    check_ecr_image() {
                        aws ecr describe-images \
                            --repository-name=${ECR_REPOSITORY} \
                            --image-ids imageTag=${IMAGE_TAG} --region ${REGION} > /dev/null 2>&1
                        return $?
                    }

                    if check_ecr_image; then
                        echo "Image with ${IMAGE_TAG} tag is already exists in registry."
                    else
                        echo "Image with ${IMAGE_TAG} tag is not exists in registry."
                        cd ${WORKSPACE}/${CODE_REPO_NAME}/app
                        aws ecr get-login-password --region ${REGION} | docker login --username AWS --password-stdin ${ECR_REGISTRY}
                        docker image build \
                        --build-arg TMDB_V3_API_KEY=${TMDB_SECRET}\
                        --tag ${ECR_REGISTRY}/${ECR_REPOSITORY}:latest \
                        --tag ${ECR_REGISTRY}/${ECR_REPOSITORY}:${IMAGE_TAG} \
                        . 
                        docker push ${ECR_REGISTRY}/${ECR_REPOSITORY}:latest
                        docker push ${ECR_REGISTRY}/${ECR_REPOSITORY}:${IMAGE_TAG}
                    fi
                '''
            }
        }
        stage('Update Manifests') {
            steps {
                withCredentials([gitUsernamePassword(credentialsId: 'nayan-gitlab', gitToolName: 'git-tool')]) {
                    sh '''
                        export ECR_REPOSITORY=${ECR_REGISTRY}/${ECR_REPOSITORY}
                        export NAMESPACE="${PREFIX}-${gitlabBranch}"

                        # cd ${WORKSPACE}

                        git clone ${MANIFESTS_REPO_URL} -b ${gitlabBranch}
                        
                        cd ${WORKSPACE}/${MANIFESTS_REPO_NAME}
                        git pull

                        cp -rf ${WORKSPACE}/${CODE_REPO_NAME}/manifests .
                        cp -rf ${WORKSPACE}/${CODE_REPO_NAME}/argo-cd .

                        sed -i -e "s|ECR_REPOSITORY|$ECR_REPOSITORY|g" \
                            -e "s|IMAGE_TAG|$IMAGE_TAG|g" \
                            ${WORKSPACE}/${MANIFESTS_REPO_NAME}/manifests/deployment.yml
                        sed -i -e "s|BRANCH|$gitlabBranch|g" \
                            -e "s|ARGOCD_NAMESPACE|$ARGOCD_NAMESPACE|g" \
                            -e "s|NAMESPACE|$NAMESPACE|g" \
                            -e "s|PREFIX|$PREFIX|g" \
                            -e "s|MANIFESTS_REPO_URL|$MANIFESTS_REPO_URL|g" \
                            ${WORKSPACE}/${MANIFESTS_REPO_NAME}/argo-cd/application.yml

                        if [ $(git status --porcelain | wc -l) -eq "0" ]; then
                            echo "No update in manifests."
                        else
                            echo "Manifests updated."
                            git add .
                            git commit -m "Commit for build #${BUILD_NUMBER}."
                            git push 
                            echo "Updated manifest pushed to repo."
                        fi
                    '''
                }
            }
        }
        stage('Deploy ArgoCD Application') {
            steps {
                    sh '''
                        aws eks update-kubeconfig --name ${K8S_CLUSTER_NAME}
                        login_argocd() {
                            echo 'Logging into the ArgoCD server.'
                            argocd login \
                                $(kubectl get service argocd-server -n ${ARGOCD_NAMESPACE} \
                                --output=jsonpath="{.status.loadBalancer.ingress[0].hostname}") \
                                --username ${ARGOCD_SECRET_USR} --password ${ARGOCD_SECRET_PSW} --insecure
                        }


                        check_argocd_app() {
                            application_name="$1"
                            argocd app get ${application_name} > /dev/null 2>&1
                            return $?
                        }

                        sync_argocd_app() {
                            application_name="$1"
                            echo "Syncing ${application_name} app in ArgoCD."
                            argocd app sync ${application_name}
                            echo "Synced ${application_name} app in ArgoCD."
                        }

                        create_argocd_app() {
                            application_name="$1"
                            echo "Creating ${application_name} app in ArgoCD."
                            cat ${WORKSPACE}/${MANIFESTS_REPO_NAME}/argo-cd/application.yml 
                            argocd app create ${application_name} \
                                --file ${WORKSPACE}/${MANIFESTS_REPO_NAME}/argo-cd/application.yml 
                            echo "Created ${application_name} app in ArgoCD."
                        }

                        argocd_deploy() {
                            login_argocd
                            application_name=$(grep -oP "^\\s*name:\\s*\\K.+" ${WORKSPACE}/${MANIFESTS_REPO_NAME}/argo-cd/application.yml | awk "{print $2}")
                            if check_argocd_app $application_name; then
                                echo "ArgoCD app ${application_name} exists."
                            else
                                echo "ArgoCD app ${application_name} doesn't exists."
                                create_argocd_app ${application_name}
                            fi
                            sync_argocd_app ${application_name}
                        }
                        
                        argocd_deploy
                    '''
            }
        }
    }

    post {
        success {
            echo 'Job completed successfully, cleaning workspace.'
            cleanWs()
        }
        failure {
            echo 'Job failed, cleaning workspace.'
            cleanWs()
        }
    }
}